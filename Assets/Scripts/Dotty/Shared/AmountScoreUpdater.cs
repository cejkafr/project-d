﻿using System.Collections.Generic;
using Dotty.Shared.Boards;

namespace Dotty.Shared
{
    public class AmountScoreUpdater : IScoreUpdater
    {
        private readonly PlayerManager playerManager;

        public AmountScoreUpdater(PlayerManager playerManager)
        {
            this.playerManager = playerManager;
        }

        public void CalculateScore(Board board)
        {
            var tempScore = new Dictionary<int, int>(playerManager.Current);
            foreach (var player in playerManager.Players)
            {
                tempScore.Add(player.Id, 0);
            }
            
            foreach (var tile in board.Tiles)
            {
                if (tile.PlayerId == -1 && tile.Amount < 1) 
                    continue;
                tempScore[tile.PlayerId] += tile.Amount;
            }

            foreach (var keyPair in tempScore)
            {
                playerManager[keyPair.Key].SetScore(keyPair.Value);
            }
        }
        
    }
}