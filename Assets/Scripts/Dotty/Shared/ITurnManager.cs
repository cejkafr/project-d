﻿namespace Dotty.Shared
{
    public interface ITurnManager
    {
        int CurrentPlayerIndex { get; }
        int ActivePlayerCount { get; }
        void Next();
        void Remove(int index);
    }
}