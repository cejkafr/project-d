﻿using UnityEngine;

namespace Dotty.Shared
{
    public class Player
    {
        public delegate void Change(Player player);

        public event Change OnPlayerScoreChanged;

        public int Id { get; }
        public string Guid { get; }
        public string Name { get; }
        public Color Color { get; }
        public int Score { get; private set; }
        public bool Active { get; private set; }

        public Player(int id, string guid, string name, Color color, int score)
        {
            Id = id;
            Guid = guid;
            Name = name;
            Color = color;
            Score = score;
            Active = true;
        }

        public void SetScore(int score)
        {
            if (score == Score) return;
            Score = score;
            OnPlayerScoreChanged?.Invoke(this);
        }

        public void Deactivate()
        {
            Active = false;
        }
    }
}