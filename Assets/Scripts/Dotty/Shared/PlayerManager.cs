﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Dotty.Shared
{
    public class PlayerManager
    {
        private static readonly Color[] PlayerColors 
            = { Color.blue, Color.green, Color.red, Color.yellow };
        
        public List<Player> Players { get; }
        public int Max { get; }
        public int Current => Players.Count;

        public Player this[int id] => id != -1 && Players.Count > id ? Players[id] : null;

        public PlayerManager(int max)
        {
            Players = new List<Player>(max);
            Max = max;
        }

        public Player GetByGuid(string guid)
        {
            foreach (var player in Players)
            {
                if (player.Guid == guid)
                    return player;
            }
            return null;
        }

        public Player Register(string guid, string name)
        {
            if (Players.Count == Max) 
                throw new Exception("Max players already joined.");
            if (GetByGuid(guid) != null)
                throw new Exception("PLayer already joined.");
            var id = Players.Count;
            var player = new Player(id, guid, name, PlayerColors[id], 0);
            Players.Add(player);
            return player;
        }

        public bool Unregister(string guid)
        {
            var player = GetByGuid(guid);
            if (player == null)
                throw new Exception("Player cannot be removed cause is not joined.");
            return Players.Remove(player);
        }

        public void Put(Player player)
        {
            if (GetByGuid(player.Guid) != null)
                Console.Write("Player with this guid already exists at different position!");
            Players.Insert(player.Id, player);
        }
    }
}