﻿using System.Collections.Generic;
using Dotty.Shared.Boards;

namespace Dotty.Shared.API
{
    public interface IGameService
    {
        ISequence Sequence { get; }
        List<IGameEvent> Events { get; }

        void Sync();

        void Connect(string guid, string name);
        void Disconnect(string guid);

        void Start();
        void Close();
        
        void Turn(sbyte2 pos);
    }
}