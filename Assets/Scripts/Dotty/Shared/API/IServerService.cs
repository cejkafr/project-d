﻿namespace Dotty.Shared.API
{
    public interface IServerService
    {
        IGameService Create(string name, string password);
        IGameService Join(string guid);
    }
}