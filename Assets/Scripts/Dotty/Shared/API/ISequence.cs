﻿namespace Dotty.Shared.API
{
    public interface ISequence
    {
        int Current { get; }

        void Increment();

        int Next();
    }
}