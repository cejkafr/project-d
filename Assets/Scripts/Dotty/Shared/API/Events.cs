﻿using Dotty.Shared.Boards;
using UnityEngine;

namespace Dotty.Shared.API
{
    public interface IGameEvent
    {
        int Sequence { get; }
    }
    
    public interface IPlayerGameEvent : IGameEvent
    {
        string Guid { get; }
    }

    public readonly struct CreateGameEvent : IGameEvent
    {
        public readonly string Guid;
        public readonly string Name;
        public readonly string Password;
        public readonly string OwnerGuid;

        public int Sequence { get; }

        public CreateGameEvent(int sequence, string guid, string name, string password, string ownerGuid)
        {
            Sequence = sequence;
            Guid = guid;
            Name = name;
            Password = password;
            OwnerGuid = ownerGuid;
        }
    }

    public readonly struct CancelGameEvent : IGameEvent
    {
        public int Sequence { get; }

        public CancelGameEvent(int sequence)
        {
            Sequence = sequence;
        }
    }
    
    public readonly struct WinGameEvent : IGameEvent
    {
        public readonly string WinnerGuid;
        
        public int Sequence { get; }

        public WinGameEvent(int sequence, string winnerGuid)
        {
            Sequence = sequence;
            WinnerGuid = winnerGuid;
        }
    }
    
    public readonly struct StartGameEvent : IGameEvent
    {
        public int Sequence { get; }
        public readonly int[] Scores;
        public readonly int NextPlayerId;

        public StartGameEvent(int sequence, int[] scores, int nextPlayerId)
        {
            Sequence = sequence;
            Scores = scores;
            NextPlayerId = nextPlayerId;
        }
    }
    
    public readonly struct BoardSnapshotEvent : IGameEvent
    {
        public readonly string Snapshot;
        
        public int Sequence { get; }

        public BoardSnapshotEvent(int sequence, string snapshot)
        {
            Sequence = sequence;
            Snapshot = snapshot;
        }
    }

    public readonly struct ConnectPlayerEvent : IPlayerGameEvent
    {
        public readonly int Id;
        public readonly string Name;
        public readonly Color Color;

        public int Sequence { get; }
        public string Guid { get; }

        public ConnectPlayerEvent(int sequence, int id, string guid, string name, Color color)
        {
            Sequence = sequence;
            Id = id;
            Guid = guid;
            Name = name;
            Color = color;
        }
    }

    public readonly struct DisconnectPlayerEvent : IPlayerGameEvent
    {
        public readonly string Reason;

        public int Sequence { get; }
        public string Guid { get; }

        public DisconnectPlayerEvent(int sequence, string guid, string reason)
        {
            Sequence = sequence;
            Guid = guid;
            Reason = reason;
        }
    }
    
    public readonly struct EliminatedPlayerEvent : IPlayerGameEvent
    {
        public int Sequence { get; }
        public string Guid { get; }

        public EliminatedPlayerEvent(int sequence, string guid)
        {
            Sequence = sequence;
            Guid = guid;
        }
    }

    public readonly struct TurnEvent : IGameEvent
    {
        public readonly int TurnNo;
        public readonly int PlayerId;
        public readonly sbyte2 Position;
        public readonly int[] Scores;
        public readonly int NextPlayerId;

        public int Sequence { get; }

        public TurnEvent(int sequence, int turnNo, int playerId, sbyte2 position, int[] scores, int nextPlayerId)
        {
            TurnNo = turnNo;
            PlayerId = playerId;
            Position = position;
            Scores = scores;
            NextPlayerId = nextPlayerId;
            Sequence = sequence;
        }
    }
}