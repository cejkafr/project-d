﻿using System.Collections.Generic;

namespace Dotty.Shared
{
    public class SequencedTurnManager : ITurnManager
    {
        private Queue<int> queue;
        
        public int CurrentPlayerIndex { get; private set; }
        public int ActivePlayerCount => queue.Count;

        public SequencedTurnManager(int playerCount)
        {
            queue = new Queue<int>(playerCount);
            for (var i = 0; i < playerCount; i++)
            {
                queue.Enqueue(i);
            }

            CurrentPlayerIndex = -1;
        }

        public void Next()
        {
            if (CurrentPlayerIndex != -1)
                queue.Enqueue(CurrentPlayerIndex);
            CurrentPlayerIndex = queue.Dequeue();
        }

        public void Remove(int index)
        {
            if (index == CurrentPlayerIndex)
            {
                CurrentPlayerIndex = -1;
                return;
            }
            
            var newQueue = new Queue<int>(queue.Count);

            while (queue.Count > 0)
            {
                var tmpIndex = queue.Dequeue();
                if (tmpIndex != index)
                    newQueue.Enqueue(tmpIndex);
            }

            queue = newQueue;
        }
    }
}