﻿using Dotty.Shared.Boards;

namespace Dotty.Shared
{
    public interface IScoreUpdater
    {
        void CalculateScore(Board board);
    }
}