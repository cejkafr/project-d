﻿using System.Collections.Generic;

namespace Dotty.Shared.Boards
{
    public class BoardTile
    {
        public int X { get; }
        public int Y { get; }
        public Board Board { get; }
        public bool Active { get; set; }
        public int PlayerId { get; set; }
        public int Amount { get; set; }

        public BoardTile(Board board, int x, int y)
        {
            X = x;
            Y = y;
            Board = board;
            Active = true;
            PlayerId = -1;
            Amount = 0;
        }

        public BoardTile(BoardTile tile)
        {
            X = tile.X;
            Y = tile.Y;
            Board = tile.Board;
            Active = tile.Active;
            PlayerId = tile.PlayerId;
            Amount = tile.Amount;
        }

        public BoardTile(Board board, int x, int y, int playerId, int amount, bool active = true)
        {
            X = x;
            Y = y;
            Board = board;
            Active = active;
            PlayerId = playerId;
            Amount = amount;
        }

        public void Set(int playerId, int amount, bool active = true)
        {
            Active = active;
            PlayerId = playerId;
            Amount = amount;
        }
        
        public List<BoardTile> FindNeighbours()
        {
            var list = new List<BoardTile>(4);
            var tile = Board[X - 1, Y];
            if (tile != null) list.Add(tile);
            tile = Board[X + 1, Y];
            if (tile != null) list.Add(tile);
            tile = Board[X, Y - 1];
            if (tile != null) list.Add(tile);
            tile = Board[X, Y + 1];
            if (tile != null) list.Add(tile);
            return list;
        }
    }
}