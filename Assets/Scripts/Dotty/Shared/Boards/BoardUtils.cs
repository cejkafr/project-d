﻿using System.Collections.Generic;

namespace Dotty.Shared.Boards
{
    public static class BoardUtils
    {
        public static List<BoardTile> FindOwnedTiles(int playerId, Board board)
        {
            var list = new List<BoardTile>();
            foreach (var tile in board.Tiles)
            {
                if (tile.PlayerId == playerId) 
                    list.Add(tile);
            }
            return list;
        }
        
        public static List<BoardTile> FindOwnedAndFreeAdjacentTiles(int playerId, Board board)
        {
            var hashSet = new HashSet<BoardTile>();
            foreach (var tile in board.Tiles)
            {
                if (tile.PlayerId == playerId)
                {
                    hashSet.Add(tile);
                    var neighbours = tile.FindNeighbours();
                    foreach (var neighbour in neighbours)
                    {
                        if (neighbour.PlayerId != -1) 
                            continue;
                        if (!hashSet.Contains(neighbour))
                            hashSet.Add(neighbour);
                    }
                }
            }

            var list = new List<BoardTile>();
            foreach (var tile in hashSet)
            {
                list.Add(tile);
            }
            return list;
        }
    }
}