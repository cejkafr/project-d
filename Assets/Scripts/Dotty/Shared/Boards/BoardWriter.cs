﻿using System.Text;
using UnityEngine.Assertions;

namespace Dotty.Shared.Boards
{
    public static partial class BoardParser
    {
        public static string WriteString(Board board)
        {
            Assert.IsNotNull(board);

            var s = new StringBuilder(WriteHeader(board.Size));
            for (sbyte y = 0; y < board.Size.y; y++)
            {
                for (sbyte x = 0; x < board.Size.x; x++)
                {
                    var tile = board[x, y];
                    if (tile.PlayerId != -1)
                        s.Append(tile.PlayerId);
                    else s.Append(Escape);
                    s.Append(tile.Amount);
                }

                s.Append(LineSeparator);
            }
            
            return s.ToString();
        }
        
        private static string WriteHeader(sbyte2 size)
        {
            return size.x + ";" + size.y + ";" + Escape + LineSeparator;
        }
    }
}