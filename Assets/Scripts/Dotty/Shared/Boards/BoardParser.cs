﻿using System;

namespace Dotty.Shared.Boards
{
    public static partial class BoardParser
    {
        private const char Escape = '#';
        private const char LineSeparator = '\n';
        private const char ValueSeparator = ';';
    }

    public struct sbyte2 : IEquatable<sbyte2>, IFormattable
    {
        public static sbyte2 Zero => new sbyte2(0, 0);
        public static sbyte2 MinusOne => new sbyte2(-1, -1);
        public static sbyte2 One => new sbyte2(1, 1);

        public sbyte x;
        public sbyte y;

        public sbyte2(sbyte x, sbyte y)
        {
            this.x = x;
            this.y = y;
        }

        public bool Equals(sbyte2 other)
        {
            return other.x == this.x && other.y == this.y;
        }

        public string ToString(string format, IFormatProvider formatProvider)
        {
            throw new NotImplementedException();
        }
    }
}