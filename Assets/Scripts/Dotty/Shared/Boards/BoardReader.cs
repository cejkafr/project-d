﻿using System;
using UnityEngine;
using UnityEngine.Assertions;

namespace Dotty.Shared.Boards
{
    public static partial class BoardParser
    {
        public static Board ReadString(string boardString)
        {
            Assert.IsNotNull(boardString);

            var lines = boardString.Trim().Split(LineSeparator);
            var header = ReadHeader(lines[0]);
            if (lines.Length - 1 != header.Size.y)
                throw new Exception("Pattern row amount doesn't match required size of " + header.Size.y + ".");

            var board = new Board(header.Size);
            for (var y = 0; y < lines.Length - 1; y++)
            {
                var line = lines[y + 1];
                if (Mathf.FloorToInt(line.Length * .5f) != header.Size.x
                    || Mathf.CeilToInt(line.Length * .5f) != header.Size.x)
                    throw new Exception("Pattern column amount at row " + y + " doesn't match required size of " +
                                        header.Size.x + ".");

                for (var x = 0; x < header.Size.x; x++)
                {
                    var substr = line.Substring(x * 2, 1);
                    var playerId = header.EscapeChar.Equals(char.Parse(substr))
                        ? (sbyte) -1
                        : sbyte.Parse(substr);
                    substr = line.Substring(x * 2 + 1, 1);
                    var amount = header.EscapeChar.Equals(char.Parse(substr))
                        ? (byte) 0
                        : byte.Parse(substr);
                    //Debug.Log("Tile " + x + "x" + y + ": owned by " + playerId + " has " + amount);
                    board.Tiles[board.CalculateArrayPos(x, y)] =
                        new BoardTile(board, (byte) x, (byte) y, playerId, amount);
                }
            }

            return board;
        }

        private static Header ReadHeader(string line)
        {
            Assert.IsNotNull(line);
            var entries = line.Split(ValueSeparator);
            if (entries.Length != 3)
                throw new Exception("Incorrect header format");
            var size = new sbyte2(sbyte.Parse(entries[0]), sbyte.Parse(entries[1]));
            var escapeChar = char.Parse(entries[2]);
            return new Header(size, escapeChar);
        }
        
        private readonly struct Header
        {
            public readonly sbyte2 Size;
            public readonly char EscapeChar;

            public Header(sbyte2 size, char escapeChar)
            {
                Size = size;
                EscapeChar = escapeChar;
            }
        }
    }
}