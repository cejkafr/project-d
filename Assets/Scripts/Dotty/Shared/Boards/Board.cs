﻿namespace Dotty.Shared.Boards
{
    public class Board
    {
        public sbyte2 Size { get; }
        public BoardTile[] Tiles { get; }

        public BoardTile this[int x, int y]
        {
            get
            {
                var pos = CalculateArrayPos(x, y);
                return pos == -1 ? null : Tiles[pos];
            }
        }

        public Board(sbyte2 size)
        {
            Size = size;
            Tiles = new BoardTile[size.x * size.y];
            for (var y = 0; y < size.y; y++)
            {
                for (var x = 0; x < size.x; x++)
                {
                    Tiles[CalculateArrayPos(x, y)] 
                        = new BoardTile(this, (byte) x, (byte) y);
                }
            }
        }

        public Board(sbyte2 size, BoardTile[] tiles)
        {
            Size = size;
            Tiles = tiles;
        }

        public Board(Board board)
        {
            Size = board.Size;
            Tiles = new BoardTile[board.Tiles.Length];
            for (var i = 0; i < board.Tiles.Length; i++)
            {
                Tiles[i] = new BoardTile(board.Tiles[i]);
            }
        }

        public Board Clone()
        {
            return new Board(this);    
        }

        public int CalculateArrayPos(int x, int y)
        {
            if (x < 0 || y < 0 || x > Size.x - 1 || y > Size.y - 1)
                return -1;
            return y * Size.y + x;
        }

        public override string ToString()
        {
            return BoardParser.WriteString(this);
        }
    }
}