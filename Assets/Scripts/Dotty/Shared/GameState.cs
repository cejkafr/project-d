﻿namespace Dotty.Shared
{
    public enum GameState
    {
        Lobby,
        InProgress,
        Finished,
        Canceled,
    }
}