﻿using Dotty.Shared.Boards;

namespace Dotty.Shared
{
    public interface ITurnCalculator
    {
        bool ProcessTurn(Board board, int playerId, sbyte2 pos);
    }
}