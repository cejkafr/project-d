﻿using Dotty.Shared.Boards;
using UnityEngine;

namespace Dotty.Shared
{
    public class StandardTurnCalculator : ITurnCalculator
    {
        private readonly bool diagonalEnabled;
        private readonly bool dumpOverflowing;

        public StandardTurnCalculator(bool diagonalEnabled, bool dumpOverflowing)
        {
            this.diagonalEnabled = diagonalEnabled;
            this.dumpOverflowing = dumpOverflowing;
        }

        public bool ProcessTurn(Board board, int playerId, sbyte2 pos)
        {
            var tile = board[pos.x, pos.y];
            if (tile == null)
                return false;
            if (!IsValidMove(playerId, tile))
                return false;
            AddAmount(playerId, tile);
            return true;
        }

        private void AddAmount(int playerId, BoardTile tile, byte depth = 0)
        {
            if (depth > 20)
            {
                Debug.Log("AddAmount force quit cause of too much nesting.");
                return;
            }

            tile.PlayerId = playerId;
            tile.Amount += 1;

            if (tile.Amount <= 5) return;

            var nTiles = tile.FindNeighbours();
            tile.Amount -= dumpOverflowing ? 5 : nTiles.Count;
            foreach (var neighbourTile in nTiles)
            {
                AddAmount(playerId, neighbourTile, depth++);
            }
        }

        private bool IsValidMove(int playerId, BoardTile tile)
        {
            if (tile.PlayerId != -1)
            {
                return tile.PlayerId == playerId;
            }

            return IsValidAdjacent(tile, playerId) 
                   || (diagonalEnabled && IsValidDiagonal(tile, playerId));
        }

        private bool IsValidAdjacent(BoardTile tile, int playerId)
        {
            return IsTileOwnedByPlayer(tile.Board[tile.X - 1, tile.Y], playerId)
                   || IsTileOwnedByPlayer(tile.Board[tile.X, tile.Y - 1], playerId)
                   || IsTileOwnedByPlayer(tile.Board[tile.X + 1, tile.Y], playerId)
                   || IsTileOwnedByPlayer(tile.Board[tile.X, tile.Y + 1], playerId);
        }

        private bool IsValidDiagonal(BoardTile tile, int playerId)
        {
            return IsTileOwnedByPlayer(tile.Board[tile.X - 1, tile.Y - 1], playerId)
                   || IsTileOwnedByPlayer(tile.Board[tile.X - 1, tile.Y + 1], playerId)
                   || IsTileOwnedByPlayer(tile.Board[tile.X + 1, tile.Y - 1], playerId)
                   || IsTileOwnedByPlayer(tile.Board[tile.X + 1, tile.Y + 1], playerId);
        }

        private static bool IsTileOwnedByPlayer(BoardTile tile, int playerId)
        {
            return tile != null && tile.PlayerId == playerId;
        }
    }
}