﻿using System.Collections;
using System.Collections.Generic;
using Dotty.Client.API;
using Dotty.Client.Renderers;
using Dotty.Server.API;
using Dotty.Shared;
using Dotty.Shared.Boards;
using UnityEngine;
using UnityEngine.Events;

namespace Dotty.Client
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private BoardRenderer boardRenderer;
        [SerializeField] private int targetFrameRate = 30;
        [SerializeField] private float syncRateSec = .5f;
        [Space]
        public UnityEvent<RemoteGame> onGameStarted = new UnityEvent<RemoteGame>();
        public UnityEvent<RemoteGame, Player> onGameWon = new UnityEvent<RemoteGame, Player>();
        public UnityEvent<Player> onPlayerConnected = new UnityEvent<Player>();
        public UnityEvent<Player, string> onPlayerDisconnected = new UnityEvent<Player, string>();
        public UnityEvent<Player, sbyte2, Board> onPlayerTurn = new UnityEvent<Player, sbyte2, Board>();

        private readonly sbyte2[] lastPlayerPositions =
            {sbyte2.MinusOne, sbyte2.MinusOne, sbyte2.MinusOne, sbyte2.MinusOne};
        private readonly string playerGuid = System.Guid.NewGuid().ToString();
        private int playerId;
        
        public RemoteGame Game { get; private set; }

        private void Awake()
        {
            Application.targetFrameRate = targetFrameRate;

            var serverService = new LocalServerService(playerGuid, "Player");
            var gameService = serverService.Create("Test", null);

            Game = new RemoteGame(gameService);
            Game.OnGameStarted += OnGameStartedHandler;
            Game.OnGameStarted += (remoteGame) => onGameStarted.Invoke(remoteGame);
            Game.OnGameWon += (remoteGame, player) => RenderBoard();
            Game.OnGameWon += (remoteGame, player) => onGameWon.Invoke(remoteGame, player);
            Game.OnPlayerConnected += OnPlayerConnectedHandler;
            Game.OnPlayerConnected += player => onPlayerConnected.Invoke(player);
            Game.OnPlayerDisconnected += (player, reason) => onPlayerDisconnected.Invoke(player, reason);
            Game.OnPlayerTurn += OnPlayerTurnHandler;
            Game.OnPlayerTurn += (player, pos, board) => onPlayerTurn.Invoke(player, pos, board);
        }

        private void Start()
        {
            Game.Start();
            StartCoroutine(SyncCoroutine());
        }

        public void TakeTurn(sbyte2 pos)
        {
            Game.Turn(pos);
        }

        private void RenderBoard()
        {
            boardRenderer.Render(Game.Board);
        }

        private void OnPlayerConnectedHandler(Player player)
        {
            if (player.Guid == playerGuid)
            {
                print("Connected with id " + player.Id);
                playerId = player.Id;
            }
        }

        private void OnGameStartedHandler(RemoteGame game)
        {
            RenderBoard();
            if (Game.TurnManager.CurrentPlayerIndex == playerId)
            {
                HighlightFreeAndAdjacentTiles();
            }  
        } 

        private void OnPlayerTurnHandler(Player player, sbyte2 pos, Board board)
        {
            ResetLastHighlights(player);
            var tile = boardRenderer[pos.x, pos.y];
            tile.IsHighlightedBorder = true;
            tile.IsHighlightedBackground = true;
            lastPlayerPositions[player.Id] = pos;
            boardRenderer.Render(Game.Board);
            if (Game.TurnManager.CurrentPlayerIndex == playerId)
            {
                HighlightFreeAndAdjacentTiles();
            }
        }

        private void ResetLastHighlights(Player player)
        {
            var lastPlayerPos = lastPlayerPositions[player.Id];
            if (lastPlayerPos.Equals(sbyte2.MinusOne)) return;
            var tile = boardRenderer[lastPlayerPos.x, lastPlayerPos.y];
            tile.IsHighlightedBorder = false;
            tile.IsHighlightedBackground = false;
        }

        private void HighlightFreeAndAdjacentTiles()
        {
            var tiles = BoardUtils.FindOwnedAndFreeAdjacentTiles(playerId, Game.Board);
            var color = Game.PlayerManager[playerId].Color;
            foreach (var boardTile in tiles)
            {
                boardRenderer[boardTile.X, boardTile.Y].SetBackgroundHighlight(color, 0.2f);
            }
        }

        private IEnumerator SyncCoroutine()
        {
            var waitForSeconds = new WaitForSecondsRealtime(syncRateSec);
            while (true)
            {
                yield return waitForSeconds;
                Game.Sync();
            }
        }
    }
}