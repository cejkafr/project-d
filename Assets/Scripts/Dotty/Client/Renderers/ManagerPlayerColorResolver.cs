﻿using UnityEngine;

namespace Dotty.Client.Renderers
{
    public class ManagerPlayerColorResolver : MonoBehaviour, IPlayerColorResolver
    {
        [SerializeField] private GameManager gameManager;
        
        public Color ResolvePlayerColor(int playerId)
        {
            return playerId == -1 ? Color.gray : gameManager.Game.PlayerManager[playerId].Color;
        }
    }
}