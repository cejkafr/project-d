﻿namespace Dotty.Client.Renderers
{
    public interface IBoardPositioner
    {
        float GetWorldPosFromTile(int tilePos);
        int GetTileFromWorldPos(float worldPos);
    }
}