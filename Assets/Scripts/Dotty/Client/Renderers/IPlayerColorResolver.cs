﻿using UnityEngine;

namespace Dotty.Client.Renderers
{
    public interface IPlayerColorResolver
    {
        Color ResolvePlayerColor(int playerId);
    }
}