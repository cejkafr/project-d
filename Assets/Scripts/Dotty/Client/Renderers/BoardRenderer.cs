﻿using System.Collections.Generic;
using Dotty.Shared;
using Dotty.Shared.Boards;
using UnityEngine;

namespace Dotty.Client.Renderers
{
    public class BoardRenderer : MonoBehaviour
    {
        [SerializeField] private GameObject tilePrefab;

        public List<BoardTileRenderer> TileRenderers { get; }
            = new List<BoardTileRenderer>(20);

        private IBoardPositioner boardPositioner;
        private IPlayerColorResolver playerColorResolver;
        private sbyte2 size;

        public BoardTileRenderer this[int x, int y]
        {
            get
            {
                var pos = CalculateArrayPos(x, y);
                return pos == -1 ? null : TileRenderers[pos];
            }
        }
        
        private void Awake()
        {
            boardPositioner = GetComponent<IBoardPositioner>();
            playerColorResolver = GetComponent<IPlayerColorResolver>();
        }

        public void Render(Board board)
        {
            size = board.Size;
            for (var y = 0; y < board.Size.y; y++)
            {
                for (var x = 0; x < board.Size.x; x++)
                {
                    var pos = board.CalculateArrayPos((byte) x, (byte) y);
                    var tileRenderer = CreateOrGetTileRenderer(pos);
                    TileRenderers[pos].transform.position = new Vector3(
                        boardPositioner.GetWorldPosFromTile(x),
                        boardPositioner.GetWorldPosFromTile(y));
                    tileRenderer.Render(board[x, y]);
                }
            }
        }

        public void Clear()
        {
            size = sbyte2.MinusOne;
            foreach (var tileRenderer in TileRenderers)
            {
                tileRenderer.gameObject.SetActive(false);
            }
        }

        private BoardTileRenderer CreateOrGetTileRenderer(int index)
        {
            if (TileRenderers.Count <= index)
                CreateTileRenderer();
            return TileRenderers[index];
        }

        private void CreateTileRenderer()
        {
            var inst = Instantiate(tilePrefab, transform);
            inst.name = "Tile_" + TileRenderers.Count;
            var tile = inst.GetComponent<BoardTileRenderer>();
            tile.Init(playerColorResolver);
            TileRenderers.Add(tile);
        }
        
        public int CalculateArrayPos(int x, int y)
        {
            if (x < 0 || y < 0 || x > size.x - 1 || y > size.y - 1)
                return -1;
            return y * size.y + x;
        }
    }
}