﻿using UnityEngine;

namespace Dotty.Client.Renderers
{
    public class StaticBoardPositioner : MonoBehaviour, IBoardPositioner
    {
        [SerializeField] private float tileOffset;

        public float GetWorldPosFromTile(int tilePos)
        {
            return tilePos * tileOffset;
        }

        public int GetTileFromWorldPos(float worldPos)
        {
            return Mathf.FloorToInt(worldPos / tileOffset);
        }
    }
}