﻿using Dotty.Shared;
using Dotty.Shared.Boards;
using UnityEngine;
using UnityEngine.UI;

namespace Dotty.Client.Renderers
{
    public class BoardTileRenderer : MonoBehaviour
    {
        [SerializeField] private Sprite[] dots;
        [SerializeField] private SpriteRenderer background;
        [SerializeField] private SpriteRenderer border;
        [SerializeField] private SpriteRenderer content;
        
        private IPlayerColorResolver playerColorResolver;

        public bool IsHighlightedBorder { get; set; }
        public bool IsHighlightedBackground { get; set; }

        public void Init(IPlayerColorResolver playerColorResolver)
        {
            this.playerColorResolver = playerColorResolver;
        }

        public void Render(BoardTile tile)
        {
            if (tile.Amount < dots.Length)
                content.sprite = dots[tile.Amount];

            var color = playerColorResolver.ResolvePlayerColor(tile.PlayerId);
            content.color = color;
            border.color = IsHighlightedBorder ? color : Color.white;
            if (IsHighlightedBackground)
            {
                var c = color;
                c.a = 0.4f;
                background.color = c;
            }
            else
            {
                background.color = Color.clear;
            }
        }

        public void SetBackgroundHighlight(Color color, float alpha = 0.4f)
        {
            var c = color;
            c.a = alpha;
            background.color = c;
        }
        
        public void SetBorderHighlight(Color color)
        {
            border.color = color;
        }
    }
}