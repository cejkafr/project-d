﻿using System;
using System.Collections.Generic;
using Dotty.Shared;
using Dotty.Shared.API;
using Dotty.Shared.Boards;

namespace Dotty.Client.API
{
    public class RemoteGameService : IGameService
    {
        public ISequence Sequence { get; }

        public List<IGameEvent> Events { get; }

        public void Sync()
        {
            throw new NotImplementedException();
        }
        

        public void Connect(string guid, string name)
        {
            throw new NotImplementedException();
        }

        public void Disconnect(string guid)
        {
            throw new NotImplementedException();
        }
        
        public void Close()
        {
            throw new NotImplementedException();
        }
        
        public void Start()
        {
            throw new NotImplementedException();
        }

        public void Turn(sbyte2 pos)
        {
            throw new NotImplementedException();
        }
    }
}