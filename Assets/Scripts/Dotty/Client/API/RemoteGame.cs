﻿using System;
using Dotty.Shared;
using Dotty.Shared.API;
using Dotty.Shared.Boards;
using BoardParser = Dotty.Shared.Boards.BoardParser;

namespace Dotty.Client.API
{
    public class RemoteGame
    {
        public delegate void GameEvent(RemoteGame game);
        public delegate void GameWonEvent(RemoteGame game, Player player);

        public delegate void PlayerEvent(Player player);

        public delegate void PlayerDisconnectedEvent(Player player, string reason);

        public delegate void PlayerTurnEvent(Player player, sbyte2 pos, Board board);

        public event GameEvent OnGameStarted;
        public event GameEvent OnGameCanceled;
        public event GameWonEvent OnGameWon;
        public event PlayerEvent OnPlayerConnected;
        public event PlayerDisconnectedEvent OnPlayerDisconnected;
        public event PlayerEvent OnPlayerEliminated;
        public event PlayerTurnEvent OnPlayerTurn;

        private readonly IGameService gameService;
        private readonly IScoreUpdater scoreUpdater;
        private readonly ITurnCalculator turnCalculator;

        private int sequence;

        public PlayerManager PlayerManager { get; }
        public ITurnManager TurnManager { get; }
        public string Guid { get; private set; }
        public string Name { get; private set; }
        public string Password { get; private set; }
        public string OwnerGuid { get; private set; }
        public GameState State { get; private set; }
        public Board Board { get; private set; }
        public int TurnNo { get; private set; }

        public RemoteGame(IGameService gameService)
        {
            this.gameService = gameService;
            turnCalculator = new StandardTurnCalculator(false, true);
            PlayerManager = new PlayerManager(4);
            TurnManager = new SequencedTurnManager(PlayerManager.Max);
            scoreUpdater = new AmountScoreUpdater(PlayerManager);
            sequence = 0;
        }

        public void Start()
        {
            gameService.Start();
            SyncEvents();
        }

        public void Turn(sbyte2 position)
        {
            if (State != GameState.InProgress)
                return;
            
            gameService.Turn(position);
            SyncEvents();
        }

        public void Sync()
        {
            gameService.Sync();
            SyncEvents();
        }

        private void SyncEvents()
        {
            for (var i = sequence; i < gameService.Events.Count; i++)
            {
                ApplyEvent(gameService.Events[i]);
                sequence++;
            }
        }

        private void ApplyEvent(IGameEvent ev)
        {
            switch (ev)
            {
                case TurnEvent eve:
                    ApplyTurnEvent(eve);
                    break;
                case BoardSnapshotEvent eve:
                    ApplyBoardSnapshotEvent(eve);
                    break;
                case StartGameEvent eve:
                    ApplyStartGameEvent(eve);
                    break;
                case CancelGameEvent eve:
                    ApplyCancelGameEvent(eve);
                    break;
                case WinGameEvent eve:
                    ApplyWinGameEvent(eve);
                    break;
                case ConnectPlayerEvent eve:
                    ApplyConnectPlayerEvent(eve);
                    break;
                case DisconnectPlayerEvent eve:
                    ApplyDisconnectPlayerEvent(eve);
                    break;
                case EliminatedPlayerEvent eve:
                    ApplyEliminatedPlayerEvent(eve);
                    break;
                case CreateGameEvent eve:
                    ApplyCreateGameEvent(eve);
                    break;
                default:
                    Console.Write("Unknown event type " + ev.GetType() + " received. Your game is now corrupted.");
                    break;
            }
        }

        private void ApplyCreateGameEvent(CreateGameEvent ev)
        {
            Name = ev.Name;
            Password = ev.Password;
            OwnerGuid = ev.OwnerGuid;
            State = GameState.Lobby;
        }

        private void ApplyStartGameEvent(StartGameEvent ev)
        {
            State = GameState.InProgress;
            scoreUpdater.CalculateScore(Board);
            TurnManager.Next();
            OnGameStarted?.Invoke(this);
        }

        private void ApplyCancelGameEvent(CancelGameEvent ev)
        {
            State = GameState.Canceled;
            OnGameCanceled?.Invoke(this);
        }

        private void ApplyWinGameEvent(WinGameEvent ev)
        {
            State = GameState.Finished;
            var player = PlayerManager.GetByGuid(ev.WinnerGuid);
            OnGameWon?.Invoke(this, player);
        }

        private void ApplyBoardSnapshotEvent(BoardSnapshotEvent ev)
        {
            Board = BoardParser.ReadString(ev.Snapshot);
        }

        private void ApplyConnectPlayerEvent(ConnectPlayerEvent ev)
        {
            var player = new Player(ev.Id, ev.Guid, ev.Name, ev.Color, 0);
            PlayerManager.Put(player);
            OnPlayerConnected?.Invoke(player);
        }

        private void ApplyDisconnectPlayerEvent(DisconnectPlayerEvent ev)
        {
            var player = PlayerManager.GetByGuid(ev.Guid);
            if (PlayerManager.Unregister(ev.Guid))
            {
                OnPlayerDisconnected?.Invoke(player, ev.Reason);
            }
        }

        private void ApplyEliminatedPlayerEvent(EliminatedPlayerEvent ev)
        {
            var player = PlayerManager.GetByGuid(ev.Guid);
            player.Deactivate();
            TurnManager.Remove(player.Id);
            OnPlayerEliminated?.Invoke(player);
        }

        private void ApplyTurnEvent(TurnEvent ev)
        {
            TurnNo = ev.TurnNo;

            turnCalculator.ProcessTurn(Board, ev.PlayerId, ev.Position);
            scoreUpdater.CalculateScore(Board);
            TurnManager.Next();

            var player = PlayerManager[ev.PlayerId];
            OnPlayerTurn?.Invoke(player, ev.Position, Board);
        }
    }
}