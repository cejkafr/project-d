﻿using UnityEngine;

namespace Dotty.Client.Controllers
{
    public class Raycaster : MonoBehaviour
    {
        [SerializeField] private Camera rayCamera;

        private Ray ray;
    
        public Vector3 GetCursorPosInWorldPos()
        {
            return rayCamera.ScreenToWorldPoint(Input.mousePosition);
        }
    }
}