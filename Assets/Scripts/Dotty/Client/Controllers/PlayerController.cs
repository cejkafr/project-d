﻿using Dotty.Client.Renderers;
using Dotty.Shared;
using Dotty.Shared.Boards;
using UnityEngine;

namespace Dotty.Client.Controllers
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private Raycaster raycaster;
        [SerializeField] private StaticBoardPositioner staticBoardPositioner;
        [SerializeField] private GameManager gameManager;

        private void Update()
        {
            if (Input.GetMouseButtonUp(0))
            {
                var cursorInWorldPos = raycaster.GetCursorPosInWorldPos();
                var pos = new sbyte2(
                    (sbyte) staticBoardPositioner.GetTileFromWorldPos(cursorInWorldPos.x),
                    (sbyte) staticBoardPositioner.GetTileFromWorldPos(cursorInWorldPos.y));
                gameManager.TakeTurn(pos);
            }
        }
    }
}