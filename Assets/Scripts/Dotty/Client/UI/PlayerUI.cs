﻿using Dotty.Shared;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Dotty.Client.UI
{
    public class PlayerUI : MonoBehaviour
    {
        [SerializeField] private Image bgImage;
        [SerializeField] private Image iconImage;
        [SerializeField] private TextMeshProUGUI nameText;
        [SerializeField] private TextMeshProUGUI scoreText;

        private Player player;
        
        public Color Color
        {
            get => iconImage.color;
            set
            {
                iconImage.color = value;
                nameText.color = value;
                scoreText.color = value;
            }
        }
        
        public string Name
        {
            get => nameText.text;
            set => nameText.text = value;
        }
        
        public int Score
        {
            get => int.Parse(scoreText.text);
            set => scoreText.text = value.ToString();
        }

        public void SetPlayer(Player player)
        {
            this.player = player;
            this.Color = player.Color;
            this.Name = player.Name;
            this.Score = player.Score;
        }

        public void Refresh()
        {
            Score = player.Score;
        }
    }
}