﻿using Dotty.Client.API;
using Dotty.Shared;
using Dotty.Shared.Boards;
using UnityEngine;

namespace Dotty.Client.UI
{
    public class PlayerManagerUI : MonoBehaviour
    {
        [SerializeField] private PlayerUI[] players;

        public void OnPlayerConnectedHandler(Player player)
        {
            var playerUi = players[player.Id];
            playerUi.SetPlayer(player);
        }

        public void OnPlayerDisconnectedHandler(Player player, string reason)
        {
            
        }

        public void OnGameStartedHandler(RemoteGame game)
        {
            foreach (var playerUi in players)
            {
                playerUi.Refresh();
            }
        }

        public void OnPlayerTurnHandler(Player player, sbyte2 pos, Board board)
        {
            foreach (var playerUi in players)
            {
                playerUi.Refresh();
            }
        }

        public void OnPlayerEliminatedHandler(Player player)
        {
            
        }
    }
}