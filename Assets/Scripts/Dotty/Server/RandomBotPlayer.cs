﻿using System;
using System.Collections.Generic;
using Dotty.Shared;
using Dotty.Shared.Boards;
using Random = UnityEngine.Random;

namespace Dotty.Server
{
    public static class RandomBotPlayer
    {
        public static sbyte2 CalculateTurn(Player player, Board board)
        {
            var ownedTiles = BoardUtils.FindOwnedAndFreeAdjacentTiles(player.Id, board);
            if (ownedTiles.Count == 0) 
                throw new Exception("No owned tiles, cannot take turn.");
            var tile = ownedTiles[Random.Range(0, ownedTiles.Count)];
            return new sbyte2((sbyte) tile.X, (sbyte) tile.Y);
        }
    }
}