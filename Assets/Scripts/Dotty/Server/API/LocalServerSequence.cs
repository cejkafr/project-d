﻿using Dotty.Shared.API;

namespace Dotty.Server.API
{
    public class LocalServerSequence : ISequence
    {
        public int Current { get; private set; }

        public LocalServerSequence(short current)
        {
            Current = current;
        }

        public void Increment()
        {
            Current += 1;
        }

        public int Next()
        {
            var current = Current;
            Current += 1;
            return current;
        }
    }
}