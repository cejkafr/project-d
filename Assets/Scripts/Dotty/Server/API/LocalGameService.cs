﻿using System;
using System.Collections.Generic;
using Dotty.Shared;
using Dotty.Shared.API;
using Dotty.Shared.Boards;

namespace Dotty.Server.API
{
    public class LocalGameService : IGameService
    {
        private readonly string localPlayerGuid;
        private readonly PlayerManager playerManager;
        private readonly ITurnManager turnManager;
        private readonly ITurnCalculator turnCalculator;
        private readonly IScoreUpdater scoreUpdater;

        private GameState state;
        private Board board;

        public int TurnNo { get; private set; }
        public ISequence Sequence { get; }
        public List<IGameEvent> Events { get; } = new List<IGameEvent>(10);

        public LocalGameService(ISequence sequence, string guid, 
            string name, string password, string playerGuid, string playerName)
        {
            localPlayerGuid = playerGuid;
            
            Sequence = sequence;
            playerManager = new PlayerManager(4);
            turnManager = new SequencedTurnManager(4);
            turnCalculator = new StandardTurnCalculator(false, true);
            scoreUpdater = new AmountScoreUpdater(playerManager);

            Events.Add(new CreateGameEvent(Sequence.Next(), guid, name, password, playerGuid));
            Connect(playerGuid, playerName);

            state = GameState.Lobby;
        }
        
        public void Sync()
        {
            if (turnManager.CurrentPlayerIndex == 0) 
                return;
            var player = playerManager[turnManager.CurrentPlayerIndex];
            var pos = RandomBotPlayer.CalculateTurn(player, board);
            ApplyTurn(player, pos);
        }

        public void Connect(string guid, string name)
        {
            if (state != GameState.Lobby)
                throw new Exception("SERVER: Cannot add new players, game not in lobby.");
            
            var player = playerManager.Register(guid, name);
            Events.Add(new ConnectPlayerEvent(Sequence.Next(), player.Id, player.Guid, player.Name, player.Color));
        }
        
        public void Disconnect(string guid)
        {
            Events.Add(new DisconnectPlayerEvent(Sequence.Next(), guid, "Theres no reason."));
        }

        
        public void Close()
        {
            Events.Add(new CancelGameEvent(Sequence.Next()));

            state = GameState.Finished;
        }
        
        public void Start()
        {
            if (state != GameState.Lobby)
                throw new Exception("SERVER: Game is not in lobby, cannot start.");

            FillWithBots();
            CreateBoard();

            SendSnapshot();
            
            TurnNo = 1;
            turnManager.Next();
            scoreUpdater.CalculateScore(board);

            Events.Add(new StartGameEvent(Sequence.Next(),ScoreToArr(), turnManager.ActivePlayerCount));
            
            state = GameState.InProgress;
        }

        private void SendSnapshot()
        {
            Events.Add(new BoardSnapshotEvent(Sequence.Next(), board.ToString()));
        }
        
        private void FillWithBots()
        {
            var freeSlots = playerManager.Max - playerManager.Current;
            for (var i = 0; i < freeSlots; i++)
            {
                Connect("bot-" + i, "Bot " + i);
            }
        }
        
        private void CreateBoard()
        {
            board = new Board(new sbyte2(12, 12));
            board[0, 0].Set(0, 1);
            board[11, 0].Set(1, 1);
            board[0, 11].Set(2, 1);
            board[11, 11].Set(3, 1);
        }

        public void Turn(sbyte2 pos)
        {
            if (state != GameState.InProgress)
                throw new Exception("SERVER: Cannot make turns when game not in progress.");
            var player = playerManager.GetByGuid(localPlayerGuid);
            ApplyTurn(player, pos);
        }

        private void ApplyTurn(Player player, sbyte2 pos)
        {
            if (turnManager.CurrentPlayerIndex != player.Id)
            {
                Console.Write("SERVER: Not players turn.");
                return;
            }

            if (!turnCalculator.ProcessTurn(board, player.Id, pos))
            {
                Console.Write("SERVER: Failed to apply player turn.");
                return;
            }

            scoreUpdater.CalculateScore(board);
            Events.Add(new TurnEvent(Sequence.Next(), TurnNo, player.Id, pos,ScoreToArr(), turnManager.ActivePlayerCount));
            TurnNo += 1;
            EliminatePlayers();
            turnManager.Next();

            if (turnManager.ActivePlayerCount < 1)
            {
                WinGame();
            }
        }

        private void EliminatePlayers()
        {
            foreach (var player in playerManager.Players)
            {
                if (!player.Active) continue;
                if (player.Score > 0) continue;
                Console.Write("SERVER: Eliminating player " + player.Id);
                turnManager.Remove(player.Id);
                player.Deactivate();
                Events.Add(new EliminatedPlayerEvent(Sequence.Next(), player.Guid));
            }
        }

        private void WinGame()
        {
            var player = playerManager[turnManager.CurrentPlayerIndex];
            state = GameState.Finished;
            Events.Add(new WinGameEvent(Sequence.Next(), player.Guid));
        }

        private int[] ScoreToArr()
        {
            var scoreArr = new int[playerManager.Max];
            foreach (var player in playerManager.Players)
            {
                scoreArr[player.Id] = player.Score;
            }
            return scoreArr;
        }
    }
}