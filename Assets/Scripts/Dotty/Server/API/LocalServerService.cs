﻿using Dotty.Shared.API;

namespace Dotty.Server.API
{
    public class LocalServerService : IServerService
    {
        private readonly string playerGuid;
        private readonly string playerName;

        public LocalServerService(string playerGuid, string playerName)
        {
            this.playerGuid = playerGuid;
            this.playerName = playerName;
        }

        public IGameService Create(string name, string password)
        {
            var sequence = new LocalServerSequence(0);
            return new LocalGameService(sequence, "local-game", name, password, playerGuid, playerName);
        }

        public IGameService Join(string guid)
        {
            throw new System.NotImplementedException();
        }
    }
}